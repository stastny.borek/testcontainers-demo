package com.cleverlance.demo.testcontainersdemo;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Test;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import static org.junit.Assert.*;

public class TestcontainersDemoApplicationTest {

    private ElasticsearchContainer esContainer
            = new ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch:7.5.1");

    @Test
    public void testESContainer() throws Exception {
        esContainer.start();

        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(HttpHost.create(esContainer.getHttpHostAddress()))
        );

        boolean pingSuccessful = client.ping(RequestOptions.DEFAULT);
        assertTrue(pingSuccessful);

        esContainer.stop();
    }
}